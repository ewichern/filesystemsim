# Readme

Filesystem simulator written in Java/JavaFX. Pre-populates each drive with a couple folders and a couple files in each folder, and the root of the drive. Number of files/folders can be changed in global variables at the beginning of `FileSystemGui.java`.

Everything is add/removed manually -- though the code will recursively add parent directories between a file/folder and the root of the drive if they do not already exist. 
* Clicking on a file should bring up info and the file "data" in the bottom center viewbox
* Files and folders can be deleted when selected from either the directory view or the folder tree
* New files or folders can be added via textbox and appropriate buttons. pathnames should contain an appropriate mount point, e.g. `/mnt/C/whatever` or if viewing the default data, `/aDifferentMountPoint/` is the mount point for the `B` drive.

* Build with maven (default target is 'package')
* Basic unit tests included for data structures.
* Woeful lack of documentation, but variables and methods are reasonably well-named.