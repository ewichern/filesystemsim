/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.odu.cs.ewichern.filesystem;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author ewichern
 */
public class Folder {

    private String folderName = "";
    private String mountLocation = "";
    private Folder parent;
    private ArrayList<Folder> subFolders;
    private ArrayList<File> files;

    public Folder(ArrayList<Object> folderContents, String folderName) {
        this.parent = null;
        this.subFolders = new ArrayList<Folder>();
        this.files = new ArrayList<File>();
        this.folderName = folderName.trim();

        for (Object item : folderContents) {
            if (item.getClass() == Folder.class) {
                this.subFolders.add((Folder) item);
            }
            if (item.getClass() == File.class) {
                this.files.add((File) item);
            }
        }
    }

    public Folder(String folderName) {
        this.parent = null;
        this.subFolders = new ArrayList<Folder>();
        this.files = new ArrayList<File>();
        this.folderName = folderName.trim();
    }

    public String getMountLocation() {
        return mountLocation;
    }

    public void setMountLocation(String mountLocation) {
        this.mountLocation = mountLocation;
    }

    public ArrayList<Object> getFolderContents() {
        ArrayList<Object> folderContents = new ArrayList<Object>();
        for (Folder folder : subFolders) {
            folderContents.add(folder);
        }
        for (File file : files) {
            folderContents.add(file);
        }
        return folderContents;
    }

    public Folder searchForPath(String pathname) {
        String[] path;
        String trimmed = pathname.trim();
        if (trimmed.equals("./") || trimmed.equals(".")) {
            return this;
        }

        int lastCharIndex = trimmed.length() - 1;

        if (trimmed.charAt(lastCharIndex) == '/') {
            trimmed = trimmed.substring(0, lastCharIndex);
        }

        if (trimmed.substring(0, 1).equals(".")) {
            trimmed = trimmed.substring(2);
            path = trimmed.split("/");
        } else {
            path = trimmed.split("/");
        }

        Folder subFolder = this.getSubFolder(path[0]);
        if (path.length > 1) {
            if (subFolder != null) {
                int slashLoc = trimmed.indexOf("/");
                String subPathname = trimmed.substring(slashLoc + 1);
                return subFolder.searchForPath(subPathname);
            }
        }
        if (path.length == 1) {
            if (subFolder != null) {
                return subFolder;
            } else if (path[0] == ".") {
                return this;
            } else if (this.hasFile(path[0])) {
                return this;
            }
        }
        return null;
    }

    public boolean hasSubFolder(Folder folder) {
        return subFolders.contains(folder);
    }

    public boolean hasSubFolder(String folderName) {
        for (Folder folder : subFolders) {
            if (folder.getFolderName().equals(folderName)) {
                return true;
            }
        }
        return false;
    }

    public Folder getSubFolder(String folderName) {
        for (Folder folder : subFolders) {
            if (folder.getFolderName().equals(folderName)) {
                return folder;
            }
        }
        return null;
    }

    public boolean hasFile(String filename) {
        for (File file : files) {
            if (file.nameEquals(filename)) {
                return true;
            }
        }
        return false;
    }

    public File getFile(String filename) {
        for (File file : files) {
            if (file.nameEquals(filename)) {
                return file;
            }
        }
        return null;
    }

    public boolean hasFile(File file) {
        return files.contains(file);
    }

    public Folder addFolder(Folder folder) {

        if (!subFolders.contains(folder)) {
            subFolders.add(folder);
            folder.setParent(this);
        }
        return folder;
    }

    public Folder removeFolder(Folder folder) {

        if (subFolders.contains(folder)) {
            folder.setParent(null);
            subFolders.remove(folder);
        }
        return this;
    }

    public File addFile(File file) {

        if (!files.contains(file)) {
            files.add(file);
            file.setParent(this);
        }

        return file;
    }

    public Folder removeFile(File file) {

        if (files.contains(file)) {
            file.setParent(null);
            files.remove(file);
        }
        return this;
    }

    public Folder getParent() {
        return parent;
    }

    public void setParent(Folder parent) {
        this.parent = parent;
    }

    public String getPath() {
        String output = "";
        if (parent == null) {
            if (!mountLocation.equals("")) {
                output = mountLocation;
            } else {
                output = "/" + folderName;
            }
        } else {
            output = parent.getPath() + "/" + folderName;
        }
        return output;
    }

    public ArrayList<String> stringArray() {
        ArrayList<String> output = new ArrayList<String>();
        ArrayList<Object> content = new ArrayList<Object>();
        content = this.getFolderContents();
        if (content != null) {
            for (Object item : content) {
                Class<?> c = item.getClass();
                String itemString = "";
                if (c == Folder.class) {
                    Folder temp = (Folder) item;
                    itemString = temp.getFolderName();
                } else {
                    File temp = (File) item;
                    itemString = temp.getFilename() + "." + temp.getExtension();
                }
                output.add(itemString);
            }
        }
        return output;
    }

    @Override
    public String toString() {
        ArrayList<Object> content = new ArrayList<Object>();
        content = this.getFolderContents();
        String output = "Folder{" + "folderName=" + folderName;
        output = output + ", content=" + content.toString() + '}';
        return output;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + (this.subFolders != null ? this.subFolders.hashCode() : 0);
        hash = 11 * hash + (this.files != null ? this.files.hashCode() : 0);
        hash = 11 * hash + (this.folderName != null ? this.folderName.hashCode() : 0);
        hash = 11 * hash + (this.parent != null ? this.parent.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj
    ) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Folder other = (Folder) obj;
        if (this.subFolders != other.subFolders && (this.subFolders == null || !this.subFolders.equals(other.subFolders))) {
            return false;
        }
        if (this.files != other.files && (this.files == null || !this.files.equals(other.files))) {
            return false;
        }
        if ((this.folderName == null) ? (other.folderName != null) : !this.folderName.equals(other.folderName)) {
            return false;
        }
        if ((this.parent == null) ? (other.parent != null) : !this.parent.equals(other.parent)) {
            return false;
        }
        return true;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName.trim();
    }
}
