package edu.odu.cs.ewichern.filesystem;

import java.util.ArrayList;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class FileSystemGui extends Application {

    GridPane grid = new GridPane();
    private static int NUM_DRIVES = 3;
    private static int NUM_FILL_FILES = 2;
    private static VFS vfs = new VFS();

    Folder currentFolder = null;
    String currentFolderPath = "";
    TextField currentFolderText = new TextField();
    ListView<String> currentFolderView = new ListView<String>();
    ObservableList<String> currentFolderList = FXCollections.observableArrayList();
    Label currentFolderLabel = new Label();
    VBox currentFolderBox = new VBox();

    File selectedFile = null;
    String selectedFilePath = "";
    ListView<String> selectedFileView = new ListView<String>();
    ObservableList<String> selectedFileList = FXCollections.observableArrayList();
    Label selectedFileLabel = new Label();
    VBox selectedFileBox = new VBox();

    ListView<String> folderTreeView = new ListView<String>();
    ObservableList<String> folderTreeList = FXCollections.observableArrayList();
    Label folderTreeLabel = new Label();
    VBox folderTreeBox = new VBox();
//    Map<String, String> folderTreeView = new TreeMap<String, String>();
//    Map<String, String> folderTreeList = new TreeMap<String, String>();

    String highlighted = "";

    final TextField newFolderPath = new TextField();
    Button newFolderButton = new Button("create folder path");
    final TextField newFilePath = new TextField();
    Button newFileButton = new Button("create file path");
    Button deleteButton = new Button("Delete highlighted file");

    private EventHandler<MouseEvent> fileInfoClickHandler = new EventHandler<MouseEvent>() {
        public void handle(MouseEvent event) {
            ListView<String> s = (ListView<String>) event.getSource();
            String sourceValue = s.getSelectionModel().getSelectedItem();
            if (sourceValue != null) {
                String pathname = currentFolderPath + "/" + sourceValue;
                System.err.println(pathname);
                highlighted = pathname;

                Folder tempFolder = vfs.findFolder(pathname);

                if (tempFolder != null) {
                    if (tempFolder != currentFolder) {
                        currentFolder = tempFolder;
                        refreshCurrentFolder();
                    } else {
                        File tempFile = currentFolder.getFile(sourceValue);
                        if (tempFile != null) {
                            selectedFile = tempFile;
                            refreshSelectedFile();
                        }
                    }
                }
            }
        }
    };

    private EventHandler<MouseEvent> folderTreeClickHandler = new EventHandler<MouseEvent>() {
        public void handle(MouseEvent event) {
            ListView<String> s = (ListView<String>) event.getSource();
            String sourceValue = s.getSelectionModel().getSelectedItem();
            if (sourceValue != null) {
                String pathname = sourceValue;
                System.err.println(pathname);
                highlighted = pathname;

                Folder tempFolder = vfs.findFolder(pathname);

                if (tempFolder != null) {
                    if (tempFolder != currentFolder) {
                        currentFolder = tempFolder;
                    }
                    if (tempFolder.getPath() != pathname) {
                        {
                            File tempFile = vfs.findFile(sourceValue);
                            if (tempFile != null) {
                                selectedFile = tempFile;
                            }
                        }
                    }
                }
            }
            refresh();
        }
    };

    @Override
    public void init() {
        for (int i = 0; i < NUM_DRIVES; i++) {
//            ListView<String> view = new ListView<String>();
//            ObservableList<String> list = FXCollections.observableArrayList();

            char driveLetter = (char) (i + 'A');
            System.err.println(driveLetter);
            Drive drive = new Drive(Character.toString(driveLetter));
            String mountLocation = "/mnt/" + driveLetter;
            if (i == 1) {
                mountLocation = "/aDifferentMountPoint";
            }
            vfs.mountDrive(mountLocation, drive);
            folderTreeList.add(mountLocation);

            for (int j = 0; j < NUM_FILL_FILES; j++) {
                String rootFile = mountLocation + "/" + driveLetter + "filename" + j + ".ext";
                folderTreeList.add(rootFile);
                vfs.addFile(rootFile);

                String folderPath = mountLocation + "/" + driveLetter + "folder" + j;
                folderTreeList.add(folderPath);
                vfs.addFolder(folderPath);

                for (int k = 0; k < NUM_FILL_FILES; k++) {
                    String filePath = folderPath + "/" + driveLetter + j + "filename" + k + ".ext";
                    folderTreeList.add(filePath);
                    vfs.addFile(filePath);
                }
            }
            System.err.println(drive);
        }

        refreshFolderTree();

        System.err.println(vfs);

        selectedFilePath = "/mnt/A/Afilename0.ext";
        selectedFile = vfs.findFile(selectedFilePath);
        refreshSelectedFile();

        currentFolder = selectedFile.getParent();
        currentFolderPath = currentFolder.getPath();
        refreshCurrentFolder();

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10.0);
        grid.setVgap(10.0);
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setGridLinesVisible(false);

        currentFolderLabel.setText("Directory listing:");
        currentFolderLabel.setFont(Font.font("Arial", 14));
        currentFolderLabel.setPrefSize(150, 50);

        selectedFileLabel.setText("Selected File info:");
        selectedFileLabel.setFont(Font.font("Arial", 14));
        selectedFileLabel.setPrefSize(150, 50);

        folderTreeLabel.setText("Folder tree overview:");
        folderTreeLabel.setFont(Font.font("Arial", 14));
        folderTreeLabel.setPrefSize(150, 50);

        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.err.println(highlighted);
                if (folderTreeList.contains(highlighted)) {
                    Folder tempFolder = vfs.findFolder(highlighted);
                    if (tempFolder.getPath().equals(highlighted)) {
                        vfs.removeFolder(highlighted);
                    } else {
                        vfs.removeFile(highlighted);
                    }
                    folderTreeList.remove(highlighted);
                }
                refreshFolderTree();
                refresh();
            }
        });
    }

    public void addTextSubmitButtonAction(final Button button, final TextField textfield) {
        button.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                String text = textfield.getText();
                System.err.println(text);
                text = text.trim();
                Object result = null;

                if (text.length() != 0) {
                    if (button.getText().contains("file")) {
                        result = vfs.addFile(text);
                        if (result != null) {
                            folderTreeList.add(text);
                            String newText = text + " ADDED!";
                            textfield.setText(newText);
                            currentFolder = ((File) result).getParent();
                        } else {
                            textfield.setText(text + " ERROR!");
                        }
                    } else {
                        result = vfs.addFolder(text);
                        if (result != null) {
                            folderTreeList.add(text);
                            String newText = text + " ADDED!";
                            textfield.setText(newText);
                            currentFolder = ((Folder) result).getParent();
                        } else {
                            textfield.setText(text + " ERROR!");
                        }
                    }
                }
                refresh();
                refreshFolderTree();
            }
        }
        );
    }

    public void refreshCurrentFolder() {
        currentFolderView.setItems(null);
        if (currentFolder != null) {
            currentFolderPath = currentFolder.getPath();
            currentFolderText.setText(currentFolderPath);
            currentFolderList.setAll(currentFolder.stringArray());
            currentFolderView.setItems(currentFolderList);
        }
    }

    public void refreshSelectedFile() {
        selectedFileView.setItems(null);
        if (selectedFile != null) {
            selectedFileList.setAll(selectedFile.stringArray());
            selectedFileView.setItems(selectedFileList);
        }
    }

    public void refreshFolderTree() {
        folderTreeView.setItems(null);
        folderTreeList.sort(null);
        folderTreeView.setItems(folderTreeList);
    }

    public void refresh() {
        refreshCurrentFolder();
        refreshSelectedFile();
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("File System Simulation");

        GridPane createArea = new GridPane();
        createArea.setHgap(10.0);
        createArea.setVgap(10.0);

        Label createNewLabel = new Label();
        createNewLabel.setText("Create a new File or Folder here");
        createNewLabel.setFont(Font.font("Arial", 14));

        Text createHint = new Text("(ex. \"./folder/subfolder\")");
        createHint.setFont(Font.font("Arial", 14));

        Label dirPathLabel = new Label();
        dirPathLabel.setText("Input path:");
        dirPathLabel.setFont(Font.font("Arial", 14));

        grid.add(createNewLabel, 1, 0, 1, 1);
        grid.add(createHint, 1, 1, 1, 1);
        grid.add(dirPathLabel, 1, 2, 1, 1);
        grid.add(newFolderPath, 1, 3, 1, 1);
        grid.add(newFolderButton, 1, 4, 1, 1);
        grid.add(newFileButton, 1, 5, 1, 1);

        selectedFileBox.getChildren().addAll(selectedFileLabel, selectedFileView);
//        selectedFileBox.setMinSize(400, 200);
        grid.add(selectedFileBox, 1, 6, 1, 1);

        ColumnConstraints column1 = new ColumnConstraints();
        ColumnConstraints column2 = new ColumnConstraints();
        ColumnConstraints column3 = new ColumnConstraints();
        ColumnConstraints column4 = new ColumnConstraints();
        ColumnConstraints column5 = new ColumnConstraints();
        column1.setPercentWidth(20);
        column2.setPercentWidth(40);
        column3.setPercentWidth(40);
        column4.setPercentWidth(20);
        column5.setPercentWidth(20);

        createArea.setGridLinesVisible(false);
//        createArea.setMinWidth(350);

        addTextSubmitButtonAction(newFileButton, newFolderPath);
        addTextSubmitButtonAction(newFolderButton, newFolderPath);

        currentFolderView.setOnMouseClicked(fileInfoClickHandler);
        folderTreeView.setOnMouseClicked(folderTreeClickHandler);

//        grid.add(createArea, 1, 0, 1, 1);
        currentFolderBox.setPrefSize(200, 580);
        currentFolderBox.getChildren().addAll(currentFolderLabel, currentFolderText, currentFolderView);
//        currentFolderBox.setPrefSize(150, 500);
        grid.add(currentFolderBox, 0, 0, 1, 7);

        folderTreeBox.getChildren().addAll(folderTreeLabel, folderTreeView, deleteButton);
//        folderTreeBox.setPrefSize(300, 500);
        grid.add(folderTreeBox, 2, 0, 1, 7);

        grid.getColumnConstraints().addAll(column1, column2, column3
        //            , column4, column5
        );

        Scene scene = new Scene(grid, 1000, 800);

        stage.setScene(scene);
        stage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
