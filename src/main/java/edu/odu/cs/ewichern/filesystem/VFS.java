/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.odu.cs.ewichern.filesystem;

import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author ewichern
 */
public class VFS {

    TreeMap<String, Drive> vfs;

    public VFS() {
        this.vfs = new TreeMap<String, Drive>();
    }

    public void mountDrive(String mountLocation, Drive drive) {
        vfs.put(mountLocation, drive);
        drive.setMountLocation(mountLocation);
    }

    public File addFile(String pathname) {
        Set<String> mountPoints = vfs.keySet();
        Drive drive = null;
        String localPath = "";
        File output = null;

        for (String mountPoint : mountPoints) {
            if (pathname.contains(mountPoint)) {
                drive = vfs.get(mountPoint);
                localPath = pathname.replaceFirst(mountPoint, ".");
            }
        }
        if (drive != null) {
            output = drive.addFile(localPath);
        }
        return output;
    }

    public Folder addFolder(String pathname) {
        Set<String> mountPoints = vfs.keySet();
        Drive drive = null;
        String localPath = "";
        Folder output = null;

        for (String mountPoint : mountPoints) {
            if (pathname.contains(mountPoint)) {
                drive = vfs.get(mountPoint);
                localPath = pathname.replaceFirst(mountPoint, ".");
            }
        }
        if (drive != null) {
            output = drive.addFolder(localPath);
        }
        return output;
    }

    public Folder removeFile(String pathname) {
        Set<String> mountPoints = vfs.keySet();
        Drive drive = null;
        String localPath = "";
        Folder output = null;

        for (String mountPoint : mountPoints) {
            if (pathname.contains(mountPoint)) {
                drive = vfs.get(mountPoint);
                localPath = pathname.replaceFirst(mountPoint, ".");
            }
        }
        if (drive != null) {
            output = drive.removeFile(localPath);
        }
        return output;
    }

    public Folder removeFolder(String pathname) {
        Set<String> mountPoints = vfs.keySet();
        Drive drive = null;
        String localPath = "";
        Folder output = null;

        for (String mountPoint : mountPoints) {
            if (pathname.contains(mountPoint)) {
                drive = vfs.get(mountPoint);
                localPath = pathname.replaceFirst(mountPoint, ".");
            }
        }
        if (drive != null) {
            output = drive.removeFolder(localPath);
        }
        return output;
    }

    public File findFile(String pathname) {
        Set<String> mountPoints = vfs.keySet();
        Drive drive = null;
        File output = null;
        String localPath = "";

        for (String mountPoint : mountPoints) {
            if (pathname.contains(mountPoint)) {
                drive = vfs.get(mountPoint);
                localPath = pathname.replaceFirst(mountPoint, ".");
            }
        }
        if (drive != null) {
            output = drive.findFile(localPath);
        }
        return output;
    }

    public Folder findFolder(String pathname) {
        Set<String> mountPoints = vfs.keySet();
        Drive drive = null;
        Folder output = null;
        String localPath = "";

        for (String mountPoint : mountPoints) {
            if (pathname.contains(mountPoint)) {
                drive = vfs.get(mountPoint);
                localPath = pathname.replaceFirst(mountPoint, ".");
            }
        }
        if (drive != null) {
            output = drive.findFolder(localPath);
        }
        return output;
    }

    public Drive getDrive(String pathname) {
        Set<String> mountPoints = vfs.keySet();
        Drive drive = null;
        String localPath = "";

        for (String mountPoint : mountPoints) {
            if (pathname.contains(mountPoint)) {
                drive = vfs.get(mountPoint);
            }
        }
        return drive;
    }

    public String printFileData(String pathname) {
        File file = findFile(pathname);
        String output = null;

        if (file != null) {
            output = file.toString();
        }
        return output;
    }

    @Override
    public String toString() {
        return "VFS{" + "vfs=" + vfs + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + (this.vfs != null ? this.vfs.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VFS other = (VFS) obj;
        if (this.vfs != other.vfs && (this.vfs == null || !this.vfs.equals(other.vfs))) {
            return false;
        }
        return true;
    }

}
