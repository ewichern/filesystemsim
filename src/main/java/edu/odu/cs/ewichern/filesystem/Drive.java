/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.odu.cs.ewichern.filesystem;

import java.util.ArrayList;

/**
 *
 * @author ewichern
 */
public class Drive {

    private Folder root;
    private String mountLocation = "";
//    private ArrayList iNodes;

    public Drive(String driveLetter) {
        root = new Folder(driveLetter);
//        iNodes = new ArrayList();
    }

    public String getMountLocation() {
        return mountLocation;
    }

    public void setMountLocation(String mountLocation) {
        this.mountLocation = mountLocation;
        root.setMountLocation(mountLocation);
    }

    public Folder findFolder(String pathname) {
        return root.searchForPath(pathname);
    }

    public File findFile(String pathname) {
        String filename = getFilename(pathname);

        Folder folderLoc = root.searchForPath(pathname);
        if (folderLoc != null) {
            return folderLoc.getFile(filename);
        }
        return null;
    }

    public Folder addFolder(String pathname) {
        String parentPath = getParentPath(pathname);
        String folderName = getFilename(pathname);

        Folder parent = root.searchForPath(parentPath);
        /**
         * if (parent != null) { return parent.addFolder(new
         * Folder(folderName)); } else { System.out.println("Specified location
         * does not exist!"); this.addFolder(parentPath); return
         * parent.addFolder(new Folder(folderName)); }
         */
        if (parent == null) {
            System.out.println("Specified location does not exist!");
            parent = this.addFolder(parentPath);
        }
        return parent.addFolder(new Folder(folderName));
    }

    public Folder removeFolder(String pathname) {
        String parentPath = getParentPath(pathname);
        String filename = getFilename(pathname);

        Folder folderLoc = root.searchForPath(pathname);
        /**
         * if (parent != null) { return parent.addFolder(new
         * Folder(folderName)); } else { System.out.println("Specified location
         * does not exist!"); this.addFolder(parentPath); return
         * parent.addFolder(new Folder(folderName)); }
         */
        if (folderLoc == null) {
            System.out.println("Specified location does not exist!");
            return null;
        }
        Folder parent = folderLoc.getParent();

        parent.removeFolder(folderLoc);

        return parent;
    }

    public Folder removeFile(String pathname) {
        String parentPath = getParentPath(pathname);
        String filename = getFilename(pathname);

        Folder folderLoc = root.searchForPath(pathname);
        /**
         * if (parent != null) { return parent.addFolder(new
         * Folder(folderName)); } else { System.out.println("Specified location
         * does not exist!"); this.addFolder(parentPath); return
         * parent.addFolder(new Folder(folderName)); }
         */
        if (folderLoc == null) {
            System.out.println("Specified location does not exist!");
            return null;
        }
        File file = folderLoc.getFile(filename);
        folderLoc.removeFile(file);

        return folderLoc;
    }

    @Override
    public String toString() {
        return "Drive{" + "root=" + root + '}';
    }

    public File addFile(String pathname) {
        String parentPath = getParentPath(pathname);
        String filename = getFilename(pathname);

        Folder parent = root.searchForPath(parentPath);
        /**
         * if (parent != null) { return parent.addFile(new File(filename)); }
         * else { System.out.println("Specified location does not exist!"); }
         */
        if (parent == null) {
            System.out.println("Specified location does not exist!");
            parent = this.addFolder(parentPath);
        }
        return parent.addFile(new File(filename));
    }

    private String getFilename(String pathname) {
        int lastSlashLoc = pathname.lastIndexOf("/");
        String filename = pathname.substring(lastSlashLoc + 1);
        return filename;
    }

    private String getParentPath(String pathname) {
        int lastSlashLoc = pathname.lastIndexOf("/");
        String parentPath = pathname.substring(0, lastSlashLoc);
        return parentPath;
    }
//    public Folder search(String filename) {
//    }

}
