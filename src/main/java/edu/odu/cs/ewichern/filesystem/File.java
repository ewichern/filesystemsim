/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.odu.cs.ewichern.filesystem;

import java.util.Random;

/**
 *
 * @author ewichern
 */
public class File {

    private Folder parent = null;
    private String filename = "";
    private String extension = "";
    private String diskLocation = "";

    private String getRandomHexString(int numchars) {
        Random r = new Random();
        StringBuilder sb = new StringBuilder();
        while (sb.length() < numchars) {
            sb.append(Integer.toHexString(r.nextInt()));
        }
        return sb.toString().substring(0, numchars);
    }

    public File(String filename, String extension, String diskLocation) {
        this.filename = filename.trim();
        this.extension = extension.trim();
        this.diskLocation = diskLocation.trim();
    }

    public File(String filename, String diskLocation) {
        String trimmed = filename.trim();
        String[] name = trimmed.split("\\.");
        this.filename = name[0];
        this.extension = name[1];
        this.diskLocation = diskLocation.trim();
    }

    public File(String filename) {
        String trimmed = filename.trim();
        String[] name = trimmed.split("\\.");

        this.filename = name[0];
        this.extension = name[1];
        this.diskLocation = getRandomHexString(80);
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        filename.trim();
        this.filename = filename;
    }

    /**
     * @return the extension
     */
    public String getExtension() {
        return extension;
    }

    /**
     * @param extension the extension to set
     */
    public void setExtension(String extension) {
        extension.trim();
        this.extension = extension;
    }

    /**
     * @return the diskLocation
     */
    public String getDiskLocation() {
        return diskLocation;
    }

    /**
     * @param diskLocation the diskLocation to set
     */
    public void setDiskLocation(String diskLocation) {
        diskLocation.trim();
        this.diskLocation = diskLocation;
    }

    public Folder getParent() {
        return parent;
    }

    public void setParent(Folder parent) {
        this.parent = parent;
    }

    public String getPath() {
        String output = "";
        output = parent.getPath() + "/" + filename + "." + extension;
        return output;
    }

    public String[] stringArray() {
        String[] output = new String[3];
        output[0] = filename;
        output[1] = extension;
        output[2] = diskLocation;
        return output;
    }

    @Override
    public String toString() {
        return "File{" + "filename=" + filename + ", extension=" + extension + ", diskLocation=" + diskLocation + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (this.filename != null ? this.filename.hashCode() : 0);
        hash = 53 * hash + (this.extension != null ? this.extension.hashCode() : 0);
        hash = 53 * hash + (this.diskLocation != null ? this.diskLocation.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final File other = (File) obj;
        if ((this.filename == null) ? (other.filename != null) : !this.filename.equals(other.filename)) {
            return false;
        }
        if ((this.extension == null) ? (other.extension != null) : !this.extension.equals(other.extension)) {
            return false;
        }
        if ((this.diskLocation == null) ? (other.diskLocation != null) : !this.diskLocation.equals(other.diskLocation)) {
            return false;
        }
        return true;
    }

    public boolean nameEquals(String filename) {

        String trimmed = filename.trim();
        String[] name = trimmed.split("\\.");
        if ((this.filename == null) ? (name[0] != null) : !this.filename.equals(name[0])) {
            return false;
        }
        if ((this.extension == null) ? (name[1] != null) : !this.extension.equals(name[1])) {
            return false;
        }
        return true;
    }

}
