package odu.edu.cs.ewichern.filesystem;

import edu.odu.cs.ewichern.filesystem.Drive;
import edu.odu.cs.ewichern.filesystem.File;
import edu.odu.cs.ewichern.filesystem.Folder;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit tests for Process Class
 *
 * @author Erik Wichern
 */
public class DriveTest extends TestCase {

    private Folder folder;
    private Folder subFolder;
    private File file;
    private Drive drive;

    private final String driveName = "C";
    private final String folderName = "testFolder";
    private final String subFolderName = "subfolder";
    private final String filename = "filename.txt";
    private final String rootPath = "./";

    @Override
    protected void setUp() {
        try {
            drive = new Drive(driveName);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        folder = new Folder(folderName);
        subFolder = new Folder(subFolderName);
        file = new File(filename);
//        String pathname = "./a/b/c/";
//        String[] path = pathname.split("/");
//
//        for (int i=0; i<path.length; i++) {
//            System.out.println(path[i]);
//    }
    }

    /**
     * Create the test case
     *
     * @param testName	driveName of the test case
     */
    public DriveTest(String testName) {
        super(testName);
    }

    /**
     * Run the suite of tests on this class
     *
     * @return the suite of tests being tested
     */
    public static Test
        suite() {
        return new TestSuite(DriveTest.class
        );
    }

    public void testConstructor() {
//		fail ("Not written yet.");
        drive = new Drive(driveName);
        assertFalse(drive == null);
    }

    public void testAddFolder() {
        String pathname = rootPath.concat(folderName);
        Folder folderLoc = drive.addFolder(pathname);

        Folder testFolder = drive.findFolder(pathname);
        assertFalse(testFolder == null);
        assertTrue(folderLoc.equals(testFolder));
    }

    public void testExplicitAddNestedFolder() {
        String pathname;
        pathname = rootPath + folderName;
        drive.addFolder(pathname);

        pathname = pathname + "/" + subFolderName;
        Folder folderLoc = drive.addFolder(pathname);

        Folder testFolder = drive.findFolder(pathname);
        assertFalse(testFolder == null);
        assertTrue(folderLoc.equals(testFolder));
    }

    public void testAddNestedFolder() {
        String pathname;
        pathname = rootPath + folderName + "/" + subFolderName;
        Folder folderLoc = drive.addFolder(pathname);

        Folder testFolder = drive.findFolder(pathname);
        System.err.println(testFolder);
        assertFalse(testFolder == null);
        assertTrue(folderLoc.equals(testFolder));
    }

    public void testExplicitAddFile() {
        String pathname;
        pathname = rootPath + folderName;
        drive.addFolder(pathname);

        pathname = pathname + "/" + subFolderName;
        drive.addFolder(pathname);

        pathname = pathname + "/" + filename;
        File newFile = drive.addFile(pathname);

        File testFile = drive.findFile(pathname);
        assertFalse(testFile == null);
        assertTrue(newFile.equals(testFile));
    }

    public void testAddFile() {
        String pathname;
        pathname = rootPath + folderName + "/" + subFolderName + "/" + filename;
        File newFile = drive.addFile(pathname);

        File testFile = drive.findFile(pathname);
        assertFalse(testFile == null);
        assertTrue(newFile.equals(testFile));
    }

    public void testRemoveFolder() {
        String pathname;
        pathname = rootPath + folderName + "/" + subFolderName;
        Folder folderLoc = drive.addFolder(pathname);

        Folder testFolder = drive.findFolder(pathname);
        assertFalse(testFolder == null);
        assertTrue(folderLoc.equals(testFolder));

        folderLoc = drive.removeFolder(pathname);

        testFolder = drive.findFolder(pathname);
        assertTrue(testFolder == null);
        assertFalse(folderLoc.equals(testFolder));
    }
//	public void testPriorityConstructor() {
//		Process process = new Process(priority);
//		assertFalse(process == null);
//		assertEquals(priority, process.getPriority());
//		assertEquals(State.READY, process.getState());
//	}
//	
//	public void testBothConstructor() {
//		Process process = new Process(priority, driveName);
//		assertFalse(process == null);
//		assertEquals(driveName, process.getName());
//		assertEquals(priority, process.getPriority());
//		assertEquals(State.READY, process.getState());
//	}
    // TODO write tests for getters/setters (once written)
}
