package odu.edu.cs.ewichern.filesystem;

import edu.odu.cs.ewichern.filesystem.Drive;
import edu.odu.cs.ewichern.filesystem.File;
import edu.odu.cs.ewichern.filesystem.Folder;
import edu.odu.cs.ewichern.filesystem.VFS;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit tests for Process Class
 *
 * @author Erik Wichern
 */
public class VFStest extends TestCase {

    private VFS vfs;
    private Folder folder;
    private Folder subFolder;
    private File file;
    private Drive drive;

    private final String driveName = "C";
    private final String folderName = "testFolder";
    private final String subFolderName = "subfolder";
    private final String filename = "filename.txt";
    private final String rootPath = "./";
    String mountPoint = "/mnt/C";

    @Override
    protected void setUp() {
        try {
            drive = new Drive(driveName);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        folder = new Folder(folderName);
        subFolder = new Folder(subFolderName);
        file = new File(filename);
        vfs = new VFS();
//        String pathname = "./a/b/c/";
//        String[] path = pathname.split("/");
//
//        for (int i=0; i<path.length; i++) {
//            System.out.println(path[i]);
//    }
    }

    /**
     * Create the test case
     *
     * @param testName	driveName of the test case
     */
    public VFStest(String testName) {
        super(testName);
    }

    /**
     * Run the suite of tests on this class
     *
     * @return the suite of tests being tested
     */
    public static Test
        suite() {
        return new TestSuite(VFStest.class);
    }

    public void testConstructor() {
//		fail ("Not written yet.");
        vfs = new VFS();
        assertFalse(drive == null);
//        System.out.println(vfs);
    }

    public void testMountDrive() {
        vfs.mountDrive(mountPoint, drive);
        String vfsToString = vfs.toString();
        assertTrue(vfsToString.contains(mountPoint));
        
        Drive testDrive = vfs.getDrive(mountPoint);
        assertEquals(drive, testDrive);
    }

    public void testAddFile() {
        vfs.mountDrive(mountPoint, drive);
        String vfsToString = vfs.toString();
        assertTrue(vfsToString.contains(mountPoint));

        String pathname;
        pathname = mountPoint + "/" + filename;
        File testFile = vfs.addFile(pathname);

        String testFilename = testFile.getFilename() + "." + testFile.getExtension();
        assertEquals(filename, testFilename);
    }

    public void testFindFile() {
        vfs.mountDrive(mountPoint, drive);
        String vfsToString = vfs.toString();
        assertTrue(vfsToString.contains(mountPoint));

        String pathname;
        pathname = mountPoint + "/" + filename;
        vfs.addFile(pathname);

        File testFile = vfs.findFile(pathname);
        assertFalse(testFile == null);
        String testFilename = testFile.getFilename() + "." + testFile.getExtension();
        assertEquals(filename, testFilename);
    }

    public void testPrintFile() {
        vfs.mountDrive(mountPoint, drive);
        String vfsToString = vfs.toString();
        assertTrue(vfsToString.contains(mountPoint));

        String pathname;
        pathname = mountPoint + "/" + filename;
        File testFile = vfs.addFile(pathname);
        assertFalse(testFile == null);
        
        String testOutput = testFile.toString();
        String printOutput = vfs.printFileData(pathname);

        System.out.println(printOutput);
        assertEquals(testOutput, printOutput);
    }
}
