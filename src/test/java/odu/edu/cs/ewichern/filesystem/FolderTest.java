package odu.edu.cs.ewichern.filesystem;

import edu.odu.cs.ewichern.filesystem.File;
import edu.odu.cs.ewichern.filesystem.Folder;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit tests for Process Class
 *
 * @author Erik Wichern
 */
public class FolderTest extends TestCase {

    private Folder folder;
    private Folder subFolder;
    private File file;

    private String folderName = "testFolder";
    private String subFolderName = "subfolder";
    private String filename = "filename.txt";

    protected void setUp() {

        folder = new Folder(folderName);
        subFolder = new Folder(subFolderName);
        file = new File(filename);
        
//        String pathname = "./a/b/c/";
//        String[] path = pathname.split("/");

//        for (int i=0; i<path.length; i++) {
//            System.out.println(path[i]);
//        }
//        String trimmed = filename.trim();
//        String[] folderName = trimmed.split("\\.");
//        System.out.println(folderName[0]);
//        System.out.println(folderName[1]);
    }

    /**
     * Create the test case
     *
     * @param testName	folderName of the test case
     */
    public FolderTest(String testName) {
        super(testName);
    }

    /**
     * Run the suite of tests on this class
     *
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FolderTest.class);
    }

    public void testConstructor() {
        Folder testFolder = new Folder(folderName);
        assertFalse(testFolder == null);
    }

    public void testAddFolder() {
        
        Folder newFolder = folder.addFolder(subFolder);
        assertTrue(folder.hasSubFolder(subFolder));
        assertTrue(newFolder.getParent().equals(folder));
    }

    public void testAddFile() {
        File newFile = folder.addFile(file);
        assertTrue(folder.hasFile(file));
        assertTrue(newFile.getParent().equals(folder));
    }

    public void testSearchForPath() {
        subFolder.addFile(file); 
        folder.addFolder(subFolder);
        Folder foundLocation = new Folder("blah");
        assertFalse(foundLocation.equals(subFolder));

        foundLocation = folder.searchForPath("./");
        System.out.println(foundLocation.toString());
        assertTrue(foundLocation.equals(folder));

        foundLocation = folder.searchForPath("./" + subFolderName);
        System.out.println(foundLocation.toString());
        assertTrue(foundLocation.equals(subFolder));

        foundLocation = folder.searchForPath("./" + subFolderName + "/" + filename);
        System.out.println(foundLocation);
        assertTrue(foundLocation.equals(file.getParent()));
    }

    public void testGetPath() {
        folder.addFolder(subFolder);
        Folder subsub = new Folder("subSub");
        subFolder.addFolder(subsub);
        
        String path = "/" + folderName + "/" + subFolderName + "/subSub";
        String testPath = subsub.getPath();
        assertEquals(path, testPath);
    }

//	public void testPriorityConstructor() {
//		Process process = new Process(priority);
//		assertFalse(process == null);
//		assertEquals(priority, process.getPriority());
//		assertEquals(State.READY, process.getState());
//	}
//	
//	public void testBothConstructor() {
//		Process process = new Process(priority, folderName);
//		assertFalse(process == null);
//		assertEquals(folderName, process.getName());
//		assertEquals(priority, process.getPriority());
//		assertEquals(State.READY, process.getState());
//	}
    // TODO write tests for getters/setters (once written)
}
